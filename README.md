# cGram

cGram is a C Telegram bot API realization

## Installation

```bash
cd ~
git clone https://gitlab.com/h3xcode/cgram.git
cd cgram
cmake . -B build
sudo cmake --build build/ --target install
```

## Example

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cgram/cgram.h>

#define TOKEN "TELEGRAM_TOKEN"

void messageListener(cBot * bot, cgram_update_t * update){
    printf("Received new message from (%s @%s): %s\n", update->message.from.first_name, update->message.from.username, update->message.text);
    cgram_replyToMessage(bot, update->message.from.id, update->message.message_id, "This is a reply to message");
    cgram_sendMessage(bot, update->message.from.id, "This is simple message");
}

void commandListener(cBot * bot, cgram_update_t * update){
    printf("Received new command from (%s @%s): %s\n", update->message.from.first_name, update->message.from.username, update->message.text);
    if (!strcmp(update->message.text, "/start")){
        char * text = (char*) malloc((strlen(update->message.from.first_name)+121)*4*sizeof(char));
        sprintf(text, "Hello, %s. This is a test bot written in cGram!", update->message.from.first_name);
        cgram_sendMessage(bot, update->message.from.id, text);
        free(text);
    } else {
        cgram_sendMessage(bot, update->message.from.id, "Command not found");
    }       
}

int main(void){
    cBot bot;
    init_cgram(&bot, TOKEN);
    cgram_setMessageListener(&bot, &messageListener); // set message listener
    cgram_setCommandListener(&bot, &commandListener); // set command listener
    cgram_user_t self_info = cgram_getMe(&bot); // get info about bot
    printf("Running as \"%s\" (@%s)\n", self_info.first_name, self_info.username);
    cgram_startPolling(&bot, 500); // polling with 500ms timeout
    return 0;
}
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GNU General Public License v3.0](https://github.com/devsec-inc/OpenPCA/blob/master/LICENSE)
