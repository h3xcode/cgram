#ifndef CGRAM_PAYMENTS_H
#define CGRAM_PAYMENTS_H

typedef struct{
    char *label;
    int amount;
} cgram_LabeledPrice_t;

typedef struct{
    char *title;
    char *description;
    char *start_parameter;
    char *currency;
    int total_amount;
} cgram_Invoice_t;

typedef struct{
    char *country_code;
    char *state;
    char *city;
    char *street_line1;
    char *street_line2;
    char *post_code;
} cgram_ShippingAddress_t;

typedef struct{
    char *name;
    char *phone_number;
    char *email;
    cgram_ShippingAddress_t shipping_address;
} cgram_OrderInfo_t;

typedef struct{
    char *id;
    char *title;
    cgram_LabeledPrice_t *prices;
} cgram_ShippingOption_t;

typedef struct{
    char *currency;
    int total_amount;
    char *invoice_payload;
    char *shipping_option_id;
    cgram_OrderInfo_t order_info;
    char *telegram_payment_charge_id;
    char *provider_payment_charge_id;
} cgram_SuccessfulPayment_t;

typedef struct{
    char *id;
    cgram_User_t from;
    char *invoice_payload;
    cgram_ShippingAddress_t shipping_address;
} cgram_ShippingQuery_t;

typedef struct{
    char *id;
    cgram_User_t from;
    char *currency;
    int total_amount;
    char *invoice_payload;
    char *shipping_option_id;
    cgram_OrderInfo_t order_info;
} cgram_PreCheckoutQuery_t;

#endif
