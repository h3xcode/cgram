#ifndef CGRAM_TYPES_H
#define CGRAM_TYPES_H
#include <cjson/cJSON.h>

typedef struct{
    long id;
    bool is_bot;
    char *first_name;
    char *last_name;
    char *username;
    char *language_code;
} cgram_User_t;

typedef struct{
    long id;
    char *first_name;
    char *last_name;
    char *title;
    char *username;
    char *type;
} cgram_Chat_t;

typedef struct{
    char *type;
    int offset;
    char *url;
    cgram_User_t user;
    char * language;
} cgram_MessageEntity_t;

typedef struct{
    char *file_id;
    char *file_unique_id;
    int width;
    int height;
    int file_size;
} cgram_PhotoSize_t;

typedef struct{
    char *file_id;
    char *file_unique_id;
    int width;
    int height;
    int duration;
    cgram_PhotoSize_t thumb;
    char *file_name;
    char *mime_type;
    int file_size;
} cgram_Animation_t;

typedef struct{
    char *file_id;
    char *file_unique_id;
    int duration;
    char *performer;
    char *title;
    char *mime_type;
    int file_size;
    cgram_PhotoSize_t PhotoSize;
} cgram_Audio_t;

typedef struct{
    char *file_id;
    char *file_unique_id;
    cgram_PhotoSize_t PhotoSize;
    char *file_name;
    char *mime_type;
    int file_size;
} cgram_Document_t;

typedef struct{
    char *file_id;
    char *file_unique_id;
    int width;
    int height;
    int duration;
    cgram_PhotoSize_t thumb;
    char *mime_type;
    int file_size;
} cgram_Video_t;

typedef struct{
    char *file_id;
    char *file_unique_id;
    int length;
    int duration;
    cgram_PhotoSize_t thumb;
    char *mime_type;
    int file_size;
} cgram_VideoNote_t;

typedef struct{
    char *file_id;
    char *file_unique_id;
    int duration;
    char *mime_type;
    int file_size;
} cgram_Voice_t;

typedef struct{
    char *phone_number;
    char *first_name;
    char *last_name;
    long user_id;
    char *vcard;
} cgram_Contact_t;

typedef struct{
    char *emoji;
    int value;
} cgram_Dice_t;

typedef struct{
    char *text;
    int voter_count;
} cgram_PollOption_t;

typedef struct{
    char *id;
    char *question;
    cgram_PollOption_t *options;
    int total_voter_count;
    bool is_closed;
    bool is_anonymous;
    char *type;
    bool allows_multiple_answers;
    int correct_option_id;
    char *explanation;
    cgram_MessageEntity_t *explanation_entities;
    int open_period;
    int close_date;
} cgram_Poll_t;

typedef struct{
    float longtitude;
    float latitude;
} cgram_Location_t;

typedef struct{
    cgram_Location_t location;
    char *title;
    char *address;
    char *foursquare_id;
    char *foursquare_type;
} cgram_Venue_t;

typedef struct{} cgram_InlineKeyboardMarkup_t;

#include "passport.h"
#include "payments.h"
#include "stickers.h"
#include "games.h"

typedef struct{
    cJSON *raw;
    long message_id;
    cgram_User_t from;
    double date;
    cgram_Chat_t chat;
    cgram_User_t forward_from;
    cgram_Chat_t forward_from_chat;
    long forward_from_message_id;
    char *forward_signature;
    char *forward_sender_name;
    double forward_date;
    char *text;
    cgram_User_t via_bot;
    cgram_MessageEntity_t *entities;
    cgram_Animation_t animation;
    cgram_Audio_t audio;
    cgram_Document_t document;
    cgram_PhotoSize_t *photo;
    cgram_Sticker_t sticker;
    cgram_Video_t video;
    cgram_VideoNote_t video_note;
    cgram_Voice_t voice;
    char *caption;
    cgram_MessageEntity_t *caption_entities;
    cgram_Contact_t contact;
    cgram_Dice_t dice;
    cgram_Game_t game;
    cgram_Poll_t poll;
    cgram_Venue_t venue;
    cgram_Location_t location;
    cgram_User_t *new_chat_members;
    cgram_User_t left_chat_member;
    char *new_chat_title;
    cgram_PhotoSize_t *new_chat_photo;
    int delete_chat_photo;
    int group_chat_created;
    int supergroup_chat_created;
    int channel_chat_created;
    long migrate_to_chat_id;
    long migrate_from_chat_id;
    cgram_Invoice_t invoice;
    cgram_SuccessfulPayment_t successful_payment;
    char *connected_website;
    cgram_PassportData_t passport_data;
    cgram_InlineKeyboardMarkup_t reply_markup;
} cgram_Message_t;

typedef struct{
    char *rawText;
    int update_id;
    cgram_Message_t message;
} cgram_Update_t;

#endif
