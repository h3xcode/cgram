/*
 * This file is part of the cGram distribution (https://github.com/h3xcode/cgram).
 * Copyright (c) 2020 DirectName (h3xcode).
 * 
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "../include/cgram/cgram.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <cjson/cJSON.h>
#include <curl/curl.h>

void msleep(const int msec){
    usleep(msec*1000);
}

char * concat(const char *s1, const char *s2)
{
    char *result = (char*) malloc(strlen(s1) + strlen(s2) + 1);
    if (result == NULL){
        fprintf(stderr, "malloc() failed\n");
        exit(EXIT_FAILURE);
    }
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}

/*
    Service section
*/
void init_cgram(cBot * bot, char * TOKEN){
    bot->TOKEN = TOKEN;
    bot->initialized = 1;
    printf("cGram (%s) initialized\n", CGRAM_VERSION);
}

void cgram_setMessageListener(cBot * bot, void * function){
    bot->messageListener = function;
}

void cgram_setCommandListener(cBot * bot, void * function){
    bot->commandListener = function;
}

char * toArray(int number){
    int n = log10(number) + 1;
    char *numberArray = calloc(n, sizeof(char));
    sprintf(numberArray, "%d", number);
    return numberArray;
}

/*
    Request section
*/

struct string {
  char *ptr;
  size_t len;
};

void init_string(struct string *s) {
  s->len = 0;
  s->ptr = malloc(s->len+1);
  if (s->ptr == NULL) {
    fprintf(stderr, "malloc() failed\n");
    exit(EXIT_FAILURE);
  }
  s->ptr[0] = '\0';
}

size_t writefunc(void *ptr, size_t size, size_t nmemb, struct string *s)
{
  size_t new_len = s->len + size*nmemb;
  s->ptr = realloc(s->ptr, new_len+1);
  if (s->ptr == NULL) {
    fprintf(stderr, "realloc() failed\n");
    exit(EXIT_FAILURE);
  }
  memcpy(s->ptr+s->len, ptr, size*nmemb);
  s->ptr[new_len] = '\0';
  s->len = new_len;

  return size*nmemb;
}

char * makeRequest(char TOKEN[], char req_text[]){
    char *request = (char*) malloc(strlen(req_text)+100*2*sizeof(char));
    if (request == NULL){
        fprintf(stderr, "malloc() failed\n");
        exit(EXIT_FAILURE);
    }
    struct string s;
    init_string(&s);
    sprintf(request, URL, TOKEN, req_text);
    CURL *curl;
    CURLcode res;
     
    curl_global_init(CURL_GLOBAL_DEFAULT);
     
    curl = curl_easy_init();
    if(curl) {
        curl_easy_setopt(curl, CURLOPT_URL, request);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &s);
     
    #ifdef SKIP_PEER_VERIFICATION
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
    #endif
     
    #ifdef SKIP_HOSTNAME_VERIFICATION
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
    #endif
        res = curl_easy_perform(curl);
        if(res != CURLE_OK)
          fprintf(stderr, "curl_easy_perform() failed: %s\n",
                  curl_easy_strerror(res));
        curl_easy_cleanup(curl);
    }
    free(request);
    curl_global_cleanup();
    return s.ptr;
 }

/*
    Parser section
*/

char * cgram_parse(const char * ret, const char * object){
    const cJSON *ch = NULL;
    cJSON *json = cJSON_Parse(ret);
    ch = cJSON_GetObjectItemCaseSensitive(json, object);
    if (cJSON_IsString(ch) && (ch->valuestring != NULL)) {
        return ch->valuestring;
    } else if (ch == NULL) {
        return "";
    } else if (cJSON_IsNumber(ch)) {
        return toArray(ch->valueint);
    } else if (cJSON_IsTrue(ch)) {
        return toArray(1);
    } else if (cJSON_IsFalse(ch)) {
        return toArray(0);
    } else {
        return cJSON_Print(ch);
    }
}

cgram_status checkStatus(cJSON * response){
    if (response == NULL){
        return (cgram_status) {.ok = 0};
    }
    if (cJSON_IsFalse(cJSON_GetObjectItem(response, "ok"))){
        return (cgram_status) {.ok = 0, .error_code = cJSON_GetObjectItem(response, "error_code")->valueint, .description = cJSON_GetObjectItem(response, "ok")->valuestring};
    }
    return (cgram_status) {.ok = 1};
}

cgram_User_t toUser(cJSON * user){
    if (user == NULL){
        return (cgram_User_t) {};
    }
    int isbot = 0;
    if (cJSON_IsTrue(cJSON_GetObjectItem(user, "is_bot"))){
        isbot = 1;
    }
    return (cgram_User_t) {.id = cJSON_GetObjectItem(user, "id")->valuedouble, .is_bot = isbot, .first_name = cgram_parse(cJSON_Print(user), "first_name"), .last_name = cgram_parse(cJSON_Print(user), "last_name"), .username = cgram_parse(cJSON_Print(user), "username"), .language_code = cgram_parse(cJSON_Print(user), "language_code")};
}

cgram_Chat_t toChat(cJSON * chat){
    if (chat == NULL){
        return (cgram_Chat_t) {};
    }
    return (cgram_Chat_t) {.id = cJSON_GetObjectItem(chat, "id")->valuedouble, .title = cgram_parse(cJSON_Print(chat), "title"), .first_name = cgram_parse(cJSON_Print(chat), "first_name"), .last_name = cgram_parse(cJSON_Print(chat), "last_name"), .username = cgram_parse(cJSON_Print(chat), "username"), .type = cgram_parse(cJSON_Print(chat), "type")};
}

cgram_Message_t toMessage(cJSON * message){
    if (message == NULL){
        return (cgram_Message_t) {};
    }
    return (cgram_Message_t) {.raw = message, .message_id = cJSON_GetObjectItem(message, "message_id")->valuedouble, .from = toUser(cJSON_GetObjectItem(message, "from")), .chat = toChat(cJSON_GetObjectItem(message, "chat")), .date = cJSON_GetObjectItem(message, "date")->valuedouble, .text = cgram_parse(cJSON_Print(message), "text")};
}

cgram_Message_t cgram_getReplyMessage(cgram_Message_t message){
    return toMessage(cJSON_GetObjectItem(message.raw, "reply_to_message"));
}

/*
    Telegram API section
*/

cgram_User_t cgram_getMe(cBot * bot){
    return toUser(cJSON_Parse(cgram_parse(makeRequest(bot->TOKEN, "getMe"), "result")));
}

cgram_Message_t cgram_sendMessage(cBot * bot, long chat_id, char text[]){
    char *request = (char*) malloc((strlen(text)+40)*3*sizeof(char));
    if (request == NULL){
        fprintf(stderr, "malloc() failed\n");
        exit(EXIT_FAILURE);
    }
    sprintf(request, "sendMessage?chat_id=%ld&text=%s", chat_id, curl_escape(text, 0));
    cJSON *message = cJSON_GetObjectItem(cJSON_Parse(makeRequest(bot->TOKEN, request)), "result");
    free(request);
    return toMessage(message);
}

cgram_Message_t cgram_replyToMessage(cBot * bot, long chat_id, long message_id, char text[]){
    char *request = (char*) malloc((strlen(text)+40)*3*sizeof(char));
    if (request == NULL){
        fprintf(stderr, "malloc() failed\n");
        exit(EXIT_FAILURE);
    }
    sprintf(request, "sendMessage?chat_id=%ld&reply_to_message_id=%ld&text=%s", chat_id, message_id, curl_escape(text, 0));
    cJSON *message = cJSON_GetObjectItem(cJSON_Parse(makeRequest(bot->TOKEN, request)), "result");
    free(request);
    return toMessage(message);
}

cgram_Message_t cgram_fsendMessage(cBot * bot, long chat_id, char text[], parseMode parse_mode){
    char *request = (char*) malloc((strlen(text)+40)*3*sizeof(char));
    if (request == NULL){
        fprintf(stderr, "malloc() failed\n");
        exit(EXIT_FAILURE);
    }
    if (parse_mode == HTML) {
        sprintf(request, "sendMessage?chat_id=%ld&parse_mode=HTML&text=%s", chat_id, curl_escape(text, 0));
    } else if (parse_mode == MarkdownV2){
        sprintf(request, "sendMessage?chat_id=%ld&parse_mode=MarkdownV2&text=%s", chat_id, curl_escape(text, 0));
    } else {
        sprintf(request, "sendMessage?chat_id=%ld&text=%s", chat_id, curl_escape(text, 0));
    }
    cJSON *message = cJSON_GetObjectItem(cJSON_Parse(makeRequest(bot->TOKEN, request)), "result");
    free(request);
    return toMessage(message);
}

cgram_Message_t cgram_freplyToMessage(cBot * bot, long chat_id, long message_id, char text[], parseMode parse_mode){
    char *request = (char*) malloc((strlen(text)+40)*3*sizeof(char));
    if (request == NULL){
        fprintf(stderr, "malloc() failed\n");
        exit(EXIT_FAILURE);
    }
    if (parse_mode == HTML) {
        sprintf(request, "sendMessage?chat_id=%ld&parse_mode=HTML&reply_to_message_id=%ld&text=%s", chat_id, message_id, curl_escape(text, 0));
    } else if (parse_mode == MarkdownV2){
        sprintf(request, "sendMessage?chat_id=%ld&parse_mode=MarkdownV2&reply_to_message_id=%ld&text=%s", chat_id, message_id, curl_escape(text, 0));
    } else {
        sprintf(request, "sendMessage?chat_id=%ld&reply_to_message_id=%ld&text=%s", chat_id, message_id, curl_escape(text, 0));
    }
    cJSON *message = cJSON_GetObjectItem(cJSON_Parse(makeRequest(bot->TOKEN, request)), "result");
    free(request);
    return toMessage(message);
}

void cgram_startPolling(cBot * bot, int sleep_time){
    bot->updates_offset = 1;
    while (1){
        char * update_raw = makeRequest(bot->TOKEN, concat("getUpdates?allowed_updates=[\"message\"]&offset=", toArray(bot->updates_offset)));
        if (!atoi(cgram_parse(update_raw, "ok"))){
            printf("Error in update: %s\n", update_raw);
            break;
        }
        cJSON *update = NULL;
        cJSON *updates = cJSON_GetObjectItem(cJSON_Parse(update_raw), "result");
        if( updates ) {
            cJSON *update = updates->child;
            while( update ) {
                char * id = cgram_parse(cJSON_Print(update), "update_id");
                bot->updates_offset = atoi(id)+1;
                cJSON *message = cJSON_GetObjectItem(update, "message");
                cgram_Message_t s_message = toMessage(message);
                cgram_Update_t s_update = {.rawText = cJSON_Print(update), .update_id = atof(id), .message = s_message};
                if (s_message.text[0] == '/'){
                    bot->commandListener(bot, &s_update);
                } else {
                    bot->messageListener(bot, &s_update);
                }              
              update = update->next;
           }
        }
        msleep(sleep_time);
    }
}

